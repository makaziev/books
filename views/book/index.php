<?php

/* @var $this yii\web\View */
/* @var $genres Genre */
/* @var $authors Authors */
/* @var $books Books */

use app\models\Authors;
use app\models\Books;
use app\models\Genre;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->registerJs(
    "
            $('.row-no-gutters').on('change keyup', '.form-horizontal', function(){
                $('#login-form').closest('form').submit();
            });
        "
);

?>

<div class="row row-no-gutters">
    <div class="col-xs-12 col-md-8">
        <?php Pjax::begin([
            'linkSelector' => '#testpjax',
            'timeout' => 3000,
            'enablePushState' => false,
        ]); ?>
        <?php
        $form = ActiveForm::begin([
            'action' => 'index',
            'method' => 'post',
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal', 'data-pjax' => ''],
        ]) ?>

        <div class="form-group">
            <div>
                <input name="q" value="<?= Yii::$app->request->post('q') ?>" type="search" class="form-control"
                       placeholder="Найти ..." style="display: inline; width: 90%">
                <?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'id' => 'testpjax']) ?>
                <p class="toggle" style="margin-top: 10px; font-size: 18px" onclick="$('.col-md-4').toggle('display')">
                    Фильтр
                    <span class="glyphicon glyphicon-triangle-bottom"></span>
                </p>
                <div class="col-md-4" style="display: none">
                    <p>Жанры</p>
                    <?php foreach ($genres as $genre) : ?>
                        <?php $checked = ''; ?>
                        <?php if (Yii::$app->request->post('genre') == $genre->id) : ?>
                            <?php $checked = 'checked'; ?>
                        <?php endif; ?>
                        <p><input <?= $checked ?> type="checkbox" name="genre" value="<?= $genre->id ?>"> <?= $genre->name ?></p>
                    <?php endforeach; ?>
                </div>
                <div class="col-md-4" style="display: none">
                    <p>Авторы</p>
                    <?php foreach ($authors as $author) : ?>
                        <?php $checked = ''; ?>
                        <?php if (Yii::$app->request->post('author') == $author->id) : ?>
                            <?php $checked = 'checked'; ?>
                        <?php endif; ?>
                        <p><input <?= $checked ?> type="checkbox" name="author" value="<?= $author->id ?>"> <?= $author->name ?></p>
                    <?php endforeach; ?>
                </div>
                <div class="col-md-4" style="display: none"></div>
            </div>
        </div>
        <?php ActiveForm::end() ?>

        <?php foreach ($books as $book) : ?>
            <h3><?php echo $book->name . '<br>'; ?></h3>
            <img src="../images/<?php echo $book->image ?>" alt="..." class="img-thumbnail" style="width: 200px">
            <footer>
                Автор:
                <?php foreach ($book->booksAuthors as $books_authors) : ?>
                    <cite title="Source Title">
                        <a href="#"><?php echo $books_authors->author->name; ?></a>
                    </cite>
                <?php endforeach; ?>
            </footer>
            <footer>
                Жанр:
                <?php foreach ($book->booksGenres as $books_genres) : ?>
                    <cite title="Source Title">
                        <a href="#"><?php echo $books_genres->genre->name; ?></a>
                    </cite>
                <?php endforeach; ?>
            </footer>
            <br>
            <small><?php echo $book->description ?></small>
            <hr>
        <?php endforeach; ?>
        <?php Pjax::end(); ?>
    </div>
</div>
