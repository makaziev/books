<?php

namespace app\controllers;

use app\models\Authors;
use app\models\Books;
use app\models\BooksAuthors;
use app\models\BooksGenres;
use app\models\Genre;
use app\models\Parser;
use Yii;
use yii\web\Controller;

class BookController extends Controller
{

    public function actionIndex()
    {
        if (Yii::$app->request->post()) {
            $dd = Yii::$app->request->post('genre');
        }

        $genres = Genre::find()->all();
        $authors = Authors::find()->with()->all();
        $books = Books::find()->all();

        $q = Yii::$app->request->post('q');
        $author = Yii::$app->request->post('author');
        $genre = Yii::$app->request->post('genre');

        if ($author && !$genre && !$q) {
            $books = Books::find()->joinWith(['booksAuthors'])->where(['books_authors.author_id' => $author])->all();
        }
        elseif (!$author && $genre && !$q) {
            $books = Books::find()->joinWith(['booksGenres'])->where(['books_genres.genre_id' => $genre])->all();
        }
        elseif (!$author && !$genre && $q) {
            $books = Books::find()->where(['like', 'name', $q])->all();
        }
        elseif ($author && !$genre && $q) {
            $books = Books::find()->joinWith(['booksAuthors'])->where(['like', 'name', $q])->andWhere(['books_authors.author_id' => $author])->all();
        }
        elseif (!$author && $genre && $q) {
            $books = Books::find()->joinWith(['booksGenres'])->where(['like', 'name', $q])->andWhere(['books_genres.genre_id' => $genre])->all();
        }
        elseif ($author && $genre && !$q) {
            $books = Books::find()->joinWith(['booksGenres', 'booksAuthors'])->where(['books_authors.author_id' => $author])->andWhere(['books_genres.genre_id' => $genre])->all();
        }
        elseif ($author && $genre && $q) {
            $books = Books::find()->joinWith(['booksGenres', 'booksAuthors'])->where(['like', 'name', $q])->andWhere(['books_authors.author_id' => $author])->andWhere(['books_genres.genre_id' => $genre])->all();
        }

//        var_dump(Books::find()->joinWith(['booksAuthors'])->where(['like', 'name', 'три'])->andWhere(['books_authors.author_id' => 444])->all());

//        if (Yii::$app->request->post('q')) {
//            $q = Yii::$app->request->post('q');
//            $books = Books::find()->where(['like', 'name', $q])->all();
//        }

        return $this->render('index', [
            'genres' => $genres,
            'books' => $books,
            'authors' => $authors,
            'dd' => $dd,
        ]);
    }

    public function actionParse()
    {
        $parser = new Parser();
        $parser->refresh_db_data();
        return $this->goBack();
    }
}
