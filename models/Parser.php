<?php


namespace app\models;


use keltstr\simplehtmldom\SimpleHTMLDom;
use yii\base\Model;

class Parser extends Model
{
    public $url = 'https://www.litmir.me/bs';

    public function refresh_db_data()
    {
        $books = Books::find()->all();
        foreach ($books as $book) {
            if (is_file('../web/images/'. $book->image)) {
//                echo '../web/images/' . $book->image;
                unlink('../web/images/'. $book->image);
            }
        }
//        die;
        $books = new Books();
        $books::deleteAll();

        $genres = new Genre();
        $genres::deleteAll();

        $authors = new Authors();
        $authors::deleteAll();

        $books_authors = new BooksAuthors();
        $books_authors::deleteAll();

        $books_genres = new BooksGenres();
        $books_genres::deleteAll();

        $dom = new SimpleHTMLDom();
        $dom = $dom::file_get_html($this->url, $use_include_path = false, $context = null, $offset = 1, $maxLen = -1, $lowercase = true, $forceTagsClosed = true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN = true, $defaultBRText = DEFAULT_BR_TEXT, $defaultSpanText = DEFAULT_SPAN_TEXT);

        foreach ($dom->find('table.island') as $value) {

            $books = new Books();
            $authors = new Authors();
            $books_authors = new BooksAuthors();
            $books_genres = new BooksGenres();

            $img_name = '';
            $img = 'https://www.litmir.me/' . $value->find('td.lt22 img', 0)->getAttribute('data-src');
//            echo $img;
//            echo '<br>';
            $book_name = $value->find('td.item .book_name', 0)->plaintext;
//            echo $book_name;
//            echo '<br>';
            $author = $value->find('.desc_box', 0)->find('a', 0)->plaintext;
            if (count($value->find('.desc_box', 0)->find('a')) > 1) {
                $author = [];
                foreach ($value->find('.desc_box', 0)->find('a') as $auth_ors) {
                    $author[] = $auth_ors->plaintext;
//                    echo $auth_ors->plaintext;
                }
            }
            else {
//                echo $author;

            }

//            echo '<br>';

            $genre = $value->find('.desc_box', 1)->find('a', 0)->plaintext;
            if (count($value->find('.desc_box', 1)->find('a')) > 1) {
                $genre = [];
                foreach ($value->find('.desc_box', 1)->find('a') as $gen_res) {
                    $genre[] = $gen_res->plaintext;
//                    echo $gen_res->plaintext;
                }
//                var_dump($genre);
            }
            else {
//                echo $genre;
            }
//            echo '<br>';

            $description = $value->find('tr', 1)->find('.description', 0)->plaintext;
//            echo $description;

            $img_name = microtime(true) . '.jpg';
            file_put_contents('../web/images/' . $img_name, file_get_contents($img));

            $books->name = $book_name;
            $books->description = $description;
            $books->image = $img_name;
            $books->save(false);

            $genre_id = 0;
            if (is_array($genre)) {
                foreach ($genre as $gen) {
                    if (strlen($gen) > 4) {
                        $genres = new Genre();
                        $books_genres = new BooksGenres();
                        if (!Genre::find()->where(['name' => $gen])->exists()) {
//                        echo "////// $gen //////";
                            $genres->name = $gen;
                            $genres->save();
                            $genre_id = $genres->id;
//                            echo "///// $genre_id /////";
                        }
                        else {
                            $genre_id = Genre::find()->where(['name' => $gen])->one()['id'];
//                            echo "///// $genre_id /////";
                        }

                        $books_genres->book_id = $books->id;
                        $books_genres->genre_id = $genre_id;
                        $books_genres->save();
                    }
                }
            }
            else {
                $genres = new Genre();
                if (!Genre::find()->where(['name' => $genre])->exists()) {
                    $genres->name = $genre;
                    $genres->save();
                    $genre_id = $genres->id;
                }
                else {
                    $genre_id = Genre::find()->where(['name' => $genre])->one()['id'];
                }
//                echo "///// $genre_id /////";
                $books_genres->book_id = $books->id;
                $books_genres->genre_id = $genre_id;
                $books_genres->save();
            }

            // authors
            $author_id = 0;
            if (is_array($author)) {
                foreach ($author as $auth) {
                    if (!Authors::find()->where(['name' => $auth])->exists()) {
                        $authors->name = $auth;
                        $authors->save();
                        $author_id = $authors->id;
                    }
                    else {
                        $author_id = Authors::find()->where(['name' => $auth])->one()['id'];
                    }
                    $books_authors->author_id = $author_id;
                    $books_authors->book_id = $books->id;
                    $books_authors->save();
                }
            }
            else {
                if (!Authors::find()->where(['name' => $author])->exists()) {
                    $authors->name = $author;
                    $authors->save();
                    $author_id = $authors->id;
                }
                else {
                    $author_id = Authors::find()->where(['name' => $author])->one()['id'];
                }
                $books_authors->author_id = $author_id;
                $books_authors->book_id = $books->id;
                $books_authors->save();
            }
//            echo '<hr>';
        }
    }
}