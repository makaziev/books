<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books_genres}}`.
 */
class m200705_183204_create_books_genres_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books_genres}}', [
            'id' => $this->primaryKey(),
            'book_id' => $this->integer(),
            'genre_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-books_genres-genre_id',
            'books_genres',
            'book_id',
            'books',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-books_genres-author_id',
            'books_genres',
            'genre_id',
            'genre',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books_genres}}');
    }
}
