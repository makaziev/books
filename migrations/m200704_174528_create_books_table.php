<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 */
class m200704_174528_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(10000),
            'image' => $this->string(1000),
//            'genre_id' => $this->integer(),
//            'author_id' => $this->integer(),
        ]);

//        $this->addForeignKey(
//            'fk-books-genre_id',
//            'books',
//            'genre_id',
//            'genre',
//            'id',
//            'CASCADE'
//        );
//
//        $this->addForeignKey(
//            'fk-books-author_id',
//            'books',
//            'author_id',
//            'authors',
//            'id',
//            'CASCADE'
//        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
